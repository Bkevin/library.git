package library;

public class Book {
	String name;
	String ISBN;
	String press;
	String author;
	String ID;
	private String status; //书籍状态-是否借出
	Double price; 
	private User borrower; //借书人

	public Book(String name, String iSBN, String press, String author, String iD, String status, Double price) {
		this.name = name;
		ISBN = iSBN;
		this.press = press;
		this.author = author;
		ID = iD;
		this.status = status;
		this.price = price;
		this.borrower = null;
	}



	@Override
	public String toString() {
		return "Book [name=" + name + ", ISBN=" + ISBN + ", press=" + press + ", author=" + author + ", ID=" + ID
				+ ", status=" + status + ", price=" + price + "]";
	}



	public User getBorrower() {
		return borrower;
	}

	public void setBorrower(User borrower) {
		this.borrower = borrower;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
