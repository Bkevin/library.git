package library;

import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

public class LibraryCentre {
	static List<Book> books;
	
	
	
	public LibraryCentre() {
		books = new ArrayList<>();
	}
	
	//添加藏书到图书馆中
	public boolean addBook(Book book) {
		if(!books.contains(book)) {
			books.add(book);
			return true;
		}
		return false;
		
	}
	
	//通过书籍名称查找书籍
	public List<Book> findBookByName(String name) {
		List<Book> foundBooks = new ArrayList<>();
		name = name.toLowerCase();
		for (Book book : books) {
			String name1 = book.name.toLowerCase();
			if(name1.indexOf(name) != -1) {
				foundBooks.add(book);
			}
		}
		return foundBooks;
	}
	
	//通过书籍ID查找书籍
	public Book findBookByID(String id) {
		for (Book book : books) {
			if(book.ID.equals(id)) {
				return book;
			}
		}
		return null;
	}
	
	//借阅书籍
	public boolean borrowBook(Book book,User user) {
		if(book.getBorrower()==null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			User.card.put(df.format(new Date()), book);
			book.setBorrower(user);
			book.setStatus("unable");
			return true;
		}
		return false;
	}
	
	//归还书籍
	public int returnBook(String returnBookID, String userLibCard) {
		Book book = findBookByID(returnBookID);

		if (book == null) {
			return -1;
		} else if (book.getBorrower() == null||userLibCard.equals(book.getBorrower().libCard) == false) {
			return 0;
		} else {
			for (Map.Entry<String,Book> e : User.card.entrySet()) {
				if(e.getValue().name.equals(book.name)) {
					User.card.remove(e.getKey());
					break;
				}
			}
			book.setBorrower(null);
			book.setStatus("Loanable");
			
			return 1;
		}
	}
	
	//展示所有藏书及其借阅状态
	public static void showAllBooks() {
		for (Book book : books) {
			System.out.println(book.toString());
		}
	}
	
	//从文件中读取所有藏书信息(未实现)
	public void getLibraryBooks(FileInputStream file) {
		try {
			ObjectInputStream in = new ObjectInputStream(file);
			Book[] preBooks = (Book[]) in.readObject();
			books = new ArrayList<>(Arrays.asList(preBooks));
			in.close();
		} catch (Exception e) {
			System.out.println("error:"+e);
		}
	}
	
	//更新藏书状态到文件中以便下次使用(未实现)
	public void updateLibraryBooks(FileOutputStream file){
		try {
			ObjectOutputStream out = new ObjectOutputStream(file);
			Book[] newBooks = (Book[]) books.toArray();
			out.writeObject(newBooks);
			out.flush();
			out.close();
		} catch (IOException e) {
			System.out.println("error:"+e);
		}
	}
	
	//从文件中读取用户借书数据(未实现)
	public void getUserData(FileInputStream file) {
		/**
		 * 从文件中读取map数据到用户类的card中
		 */
	}
	
	//更新用户借书数据到文件中以便下次使用(未实现)
	public void getUserData(FileOutputStream file) {
		/**
		 * 将用户类中的map类card写到文件中
		 */
	}
}
