package library;

import java.util.List;

public class Menu {
	
	//初始化图书馆中藏书
	static {
		Book b1 = new Book("三体", "102122035", "重庆出版社", "刘慈欣","1", "Loanable", 50.20);
		Book b2 = new Book("盗墓笔记（典藏版共9册）", "9787807407270", "上海文化出版社", "南派三叔","2", "Loanable", 230.90);
		Book b3 = new Book("JAVA JDK9 学习笔记", "9787302501183", "清华大学出版社", "林信良","3", "Loanable", 83.30);
		Book b4 = new Book("Python科学计算(第2版)（附光盘）", "9787302426585", "清华大学出版社", "刘慈欣","4", "Loanable", 100.30);
		Book b5 = new Book("时间简史（插图版）", "9787535732309", "湖南科学技术出版社", "史蒂芬·霍金","5", "Loanable", 29.00);
		Book b6 = new Book("三国演义（套装上下册）", "9787020008728", "人民文学出版社", "罗贯中","6", "Loanable", 27.20);
		Book b7 = new Book("Java核心技术 卷I 基础知识（原书第11版）", "9787111636663", "机械工业出版社", "凯·S.霍斯特曼","7", "Loanable", 109.30);
		Book b8 = new Book("阿里巴巴Java开发手册（第2版）", "9787121395925", "电子工业出版社", "杨冠宝","8", "Loanable", 44.10);
		Book b9 = new Book("Python基础教程（第3版）", "9787115474889", "人民邮电出版社", "Magnus Lie Hetland","9", "Loanable", 73.70);
		Book b10 = new Book("把时间当作朋友（第3版 全彩）", "9787121210273", "电子工业出版社", "李笑来","10", "Loanable", 38.20);

		LibraryCentre.books.add(b1);
		LibraryCentre.books.add(b2);
		LibraryCentre.books.add(b3);
		LibraryCentre.books.add(b4);
		LibraryCentre.books.add(b5);
		LibraryCentre.books.add(b6);
		LibraryCentre.books.add(b7);
		LibraryCentre.books.add(b8);
		LibraryCentre.books.add(b9);
		LibraryCentre.books.add(b10);
	}
	
	public static void menu() {
		System.out.println("***********MENU**********");
		System.out.println("*\t <1>查看藏书 \t*");
		System.out.println("*\t <2>查找书籍 \t*");
		System.out.println("*\t <3>借阅书籍 \t*");
		System.out.println("*\t <4>查看已借阅书籍 \t*");
		System.out.println("*\t <5>归还书籍 \t*");
		System.out.println("*\t <6>退出            \t*");
		System.out.println("*************************");
		System.out.print("请输入你的选择：");
	}
	
	public static void showLibraryBooks() {
		LibraryCentre.showAllBooks();
	}
}
