package library;
import java.util.*;
import java.io.*;
public class Main {

	public static void main(String[] args) {
		String libCardNumber;
		Scanner sc = new Scanner(System.in);
		LibraryCentre lib = new LibraryCentre();
		
		/*
		//从文件中读取图书馆藏书数据(未实现)
		FileInputStream libraryFileIn = null;
		try {
			libraryFileIn = new FileInputStream("D://library.txt");
			lib.getLibraryBooks(libraryFileIn);
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				if(libraryFileIn != null) {
					libraryFileIn.close();
				}
			} catch (IOException e) {
				System.out.println("error:"+e);
			}
		}
		*/
		
		//用户输入借书证号创建新用户文档(未实现)
		System.out.print("请输入你的借书证号：");
		libCardNumber = sc.nextLine();
		User user = new User(libCardNumber);
		/*
		FileInputStream userFileIn = null;
		//如果用户不存在则为用户创建一个新的借书文档(未实现)
		try {
			userFileIn = new FileInputStream("D://"+libCardNumber+".txt");
			lib.getUserData(userFileIn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(userFileIn != null) {
					userFileIn.close();
				}
			} catch (IOException e) {
				System.out.println("error:"+e);
			}
		}
		*/
		
		Menu.menu();
		while(true) {
			int choice = Integer.valueOf(sc.nextLine());
			
			switch (choice) {
			case 1:
				Menu.showLibraryBooks();
				break;
			case 2:
				System.out.println("清输入查找方式：");
				System.out.println("<1>通过书籍名称查找");
				System.out.println("<2>通过书籍ID查找");
				int findMode = Integer.valueOf(sc.nextLine());
				if (findMode == 1) {
					System.out.print("请输入书籍名称：");
					String bookName = sc.nextLine();
					List<Book> foundBook = lib.findBookByName(bookName);
					if(foundBook == null) {
						System.out.println("查无此书！！！");
					}else {
						for (Book book : foundBook) {
							System.out.println(book.toString());
						}
						System.out.println("清输入你想借阅的书籍ID，输入-1返回");
						String borrowChoice = sc.nextLine();
						if(!borrowChoice.equals("-1")) {
							Book borrowBook = lib.findBookByID(borrowChoice);
							if (borrowBook == null) {
								System.out.println("输入的ID有误！！！");
							} else {
								if (!lib.borrowBook(borrowBook, user)) {
									System.out.println("借阅失败！该书已借出！");
								} else {
									System.out.println("借阅成功！");
								}
							}
						}
					}
				} else if (findMode == 2) {
					System.out.print("请输入书籍ID：");
					String bookID = sc.nextLine();
					Book foundBook = lib.findBookByID(bookID);
					if(foundBook == null) {
						System.out.println("查无该书！！！");
					}else {
						System.out.println(foundBook.toString());
						System.out.println("是否要借阅该书？   <1>是   <2>否");
						System.out.print("请输入你的选择：");
						int borrowChoice = Integer.valueOf(sc.nextLine());
						if (borrowChoice == 1) {
							if (!lib.borrowBook(foundBook, user)) {
								System.out.println("借阅失败！该书已借出！");
							} else {
								System.out.println("借阅成功！");
							}
						}
					}
				}
				break;
			case 3:
				System.out.print("清输入书籍ID：");
				String bookID = sc.nextLine();
				Book book = lib.findBookByID(bookID);
				if (book == null) {
					System.out.println("查无此书！！！");
				} else {
					if (!lib.borrowBook(book, user)) {
						System.out.println("借阅失败！该书已借出！");
					} else {
						System.out.println("借阅成功！");
					}
				}
				break;
			case 4:
				user.checkBorrowedBook();
				break;
			case 5:
				if(user.checkBorrowedBook()==1) {
					System.out.print("清输入要归还的书籍ID：");
					String returnBookID = sc.nextLine();
					int bookReturnStatus = lib.returnBook(returnBookID, user.libCard);
					if (bookReturnStatus == -1) {
						System.out.println("该书不存在！");
					} else if (bookReturnStatus == 0) {
						System.out.println("你尚未借阅该书！");
					} else {
						System.out.println("归还成功！");
					}
				}
				break;
			case 6:
				System.out.println("感谢本次使用。欢迎下次光临！");
				sc.close();
				/*将图书馆中所有藏书状态更新到文件中以便下次使用(未实现)
				FileOutputStream libraryFileOut = null;
				try {
					libraryFileOut = new FileOutputStream("C://library.txt");
					lib.updateLibraryBooks(libraryFileOut);
					
				} catch (FileNotFoundException e) {
					System.out.println("error:"+e);
				} finally {
					try {
						if(libraryFileOut != null) {
							libraryFileOut.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				*/
				System.exit(0);
				break;
			default:
				System.out.println("输入错误！清重新输入！");
				break;
			}
			System.out.println();
			System.out.println();
			System.out.println();
			Menu.menu();
		}
	}

}
